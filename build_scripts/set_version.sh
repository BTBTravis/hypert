#!/usr/bin/env bash

CURRENT_VERSION=$(cat hypert/mix.exs | grep version | awk -F'"' '{print $2}')

echo Current Version: ${CURRENT_VERSION}
echo What would you like to update version to?
read VERSION

echo Updating to ${VERSION}

sed -i '' "7s/${CURRENT_VERSION}/${VERSION}/g" ./hypert/mix.exs
sed -i '' "11s/${CURRENT_VERSION}/${VERSION}/g" ./hypert/Dockerfile
