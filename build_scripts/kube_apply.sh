#!/usr/bin/env ash

IMG=${1}
export IMAGE=${IMG}
# for f in templates/*.yml
# do
#  envsubst < $f > “.generated/$(basename $f)”
# done
# kubectl apply -f .generated/
mkdir -p .generated
envsubst < ./kubeconfigs/web.deployment.yml > ./.generated/web.deployment.yml
kubectl apply -f .generated/ --insecure-skip-tls-verify=true
