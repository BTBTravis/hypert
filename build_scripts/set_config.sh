#!/usr/bin/env ash

BASE=$(cat <<EOF
use Mix.Config
config :hypert, HypertWeb.Endpoint,
  secret_key_base: "KEY"
EOF
)

echo "${BASE}" | sed "s/KEY/${HYPERT_HASH_KEY}/g" > ./hypert/config/prod.secret.exs
