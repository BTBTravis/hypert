# Hyperthymesia API

A link store to off load links from memory and be reminded of them at a later date.


## Dev

### Get server up and running locally

1. Git clone
1. Start postgres and adminir containers with `cd hyperthymesia && docker-compose up`
1. Then start Phoenix server:
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate` or `mix ecto.setup` 
  * Start Phoenix endpoint with `mix phx.server`
 
### Sample API calls

Create a link: `curl -X POST --data '{"link":{"url":"http://www.westwing.de","title":"Westwing","break":"1w"}}' -H 'content-type: application/json' -H 'Accept: application/json' -H 'Authorization: key xxxx' 'localhost:4000/api/links'`

Get list of links: `curl -X GET -H 'Accept: application/json' -H 'Authorization: key xxxx' 'http://0.0.0.0:4000/api/links/'` 

Get link by id: `curl -X GET -H 'Accept: application/json' -H 'Authorization: key xxxx' 'http://0.0.0.0:4000/api/links/2'`


### Helpful dev links
  * Official website: http://www.phoenixframework.org/
  * Basic Elixer Starter Docs: https://elixir-lang.org/getting-started/basic-types.html#linked-lists
  * Phx Docs: https://hexdocs.pm/phoenix/contexts.html#thinking-about-design
  * Ecto Docs: https://hexdocs.pm/ecto/Ecto.Query.html#content
  * Elixer Docs: https://hexdocs.pm/elixir/Keyword.html#t:t/0
  * Distillery "Deploying With Docker": https://hexdocs.pm/distillery/guides/working_with_docker.html
  * Distillery "Phoenix**: https://hexdocs.pm/distillery/guides/phoenix_walkthrough.html
  * Production guide: http://www.phoenixframework.org/docs/deployment
  * Local Adminer: http://localhost:8080/?pgsql=postgres&username=devpostgres&db=hypert_local_dev&ns=public&select=links
  * Elastic Search Queries: https://dzone.com/articles/23-useful-elasticsearch-example-queries
  * Elixir Recompile: https://stackoverflow.com/questions/36490089/how-do-i-recompile-an-elixir-project-and-reload-it-from-within-iex


## Production

Live Link: https://hypert.btbbackend.tech/

### Test production locally

1. Start postgres and adminir containers with `cd hyperthymesia && docker-compose up`
1. Then in the `hyperthymesia/hypert` dir build and compile files for release
   with: `MIX_ENV=prod mix compile && MIX_ENV=prod mix release`
1. Run production build locally with `(source dev_export.sh;
   _build/prod/rel/hypert/bin/hypert foreground)` 
   `soucre dev_export.sh` exports a copy of the dev.exs config for the database
   config and endpoint config. The prens mean it will do so in a subshell and
   not pollute your current shell

### Deploy to production

This app is designed to be deoplyed via docker containers, one conatiner for the
app itself and one for postgres. To build this container simply run `make build`
in `/hypert`. My production pipeline goes as follows:

1. copy `/hypert/docker-compose.yml` to production server 
1. `$(aws ecr get-login --no-include-email --region eu-central-1)` to login to
AWS's elastic container repositary 
1. `docker tag hypert:latest 853019563312.dkr.ecr.eu-central-1.amazonaws.com/hypert:latest`
so ECR reconigzes the container 
1. finally `docker push 853019563312.dkr.ecr.eu-central-1.amazonaws.com/hypert:latest`


### Database recovery
*2019-02-02:* When moving to k8s from docker swarm the database had to be
   recovered from a backup of the hypert-data postgres volume. Here are the steps to reproduce.
   
   1. untar the backup `tar -xvf hypert-1546683986.tar`
   1. use docker compose with adminer to spin up a new postgres container and
      volume bind the extracted backup
   1. Login to the postgres db via adminer with the live creds
   1. Use adminer to exprot the links table into an insert query sql file.
   1. copy the query file into the live container `kubectl exec -it hypert-data-7df59998bd-l8kh2 bash`
   1. ssh into the live container `kubectl exec -it hypert-data-7df59998bd-l8kh2 bash`
   1. from inside the live container run the query with `psql -U prod -d hypert_prod -a -f query.sql`

Lernings: AWS RDS might be worth the $$$ for backups and easier recovery
