defmodule Hypert.MemoryTest do
  use Hypert.DataCase

  alias Hypert.Memory

  describe "links" do
    alias Hypert.Memory.Link

    @valid_attrs %{break_date: ~N[2010-04-17 14:00:00.000000], title: "some title", url: "some url"}
    @update_attrs %{break_date: ~N[2011-05-18 15:01:01.000000], title: "some updated title", url: "some updated url"}
    @invalid_attrs %{break_date: nil, title: nil, url: nil}

    def link_fixture(attrs \\ %{}) do
      {:ok, link} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Memory.create_link()

      link
    end

    test "list_links/0 returns all links" do
      link = link_fixture()
      assert Memory.list_links() == [link]
    end

    test "get_link!/1 returns the link with given id" do
      link = link_fixture()
      assert Memory.get_link!(link.id) == link
    end

    test "create_link/1 with valid data creates a link" do
      assert {:ok, %Link{} = link} = Memory.create_link(@valid_attrs)
      assert link.break_date == ~N[2010-04-17 14:00:00.000000]
      assert link.title == "some title"
      assert link.url == "some url"
    end

    test "create_link/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Memory.create_link(@invalid_attrs)
    end

    test "update_link/2 with valid data updates the link" do
      link = link_fixture()
      assert {:ok, link} = Memory.update_link(link, @update_attrs)
      assert %Link{} = link
      assert link.break_date == ~N[2011-05-18 15:01:01.000000]
      assert link.title == "some updated title"
      assert link.url == "some updated url"
    end

    test "update_link/2 with invalid data returns error changeset" do
      link = link_fixture()
      assert {:error, %Ecto.Changeset{}} = Memory.update_link(link, @invalid_attrs)
      assert link == Memory.get_link!(link.id)
    end

    test "delete_link/1 deletes the link" do
      link = link_fixture()
      assert {:ok, %Link{}} = Memory.delete_link(link)
      assert_raise Ecto.NoResultsError, fn -> Memory.get_link!(link.id) end
    end

    test "change_link/1 returns a link changeset" do
      link = link_fixture()
      assert %Ecto.Changeset{} = Memory.change_link(link)
    end
  end
end
