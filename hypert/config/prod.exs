use Mix.Config

# For production, we often load configuration from external
# sources, such as your system environment. For this reason,
# you won't find the :http configuration below, but set inside
# HypertWeb.Endpoint.init/2 when load_from_system_env is
# true. Any dynamic configuration should be done there.
#
# Don't forget to configure the url host to something meaningful,
# Phoenix uses this information when generating URLs.
#
# Finally, we also include the path to a cache manifest
# containing the digested version of static files. This
# manifest is generated by the mix phx.digest task
# which you typically run after static files are built.

# port replaced at runtime with env var PORT
config :hypert, HypertWeb.Endpoint,
  load_from_system_env: true,
  # http: [port: "${PORT}"],
  # url: [host: "localhost", port: "${PORT}"], # This is critical for ensuring web-sockets properly authorize.
  server: true,
  root: ".",
  version: Application.spec(:hypert, :vsn)


# Do not print debug messages in production
config :logger, level: :info

# Configure your database
config :hypert, Hypert.Repo,
  load_from_system_env: true,
  adapter: Ecto.Adapters.Postgres

config :hypert, Hypert.Scheduler,
  jobs: [
    # {"@daily", {Hypert.Memory, :refresh_links, []}}
    #{"*/60 * * * *", {Hypert.Memory.Search, :index_links, []}}
  ]
#   username: "devpostgres",
#   password: "dev",
#   database: "hypert_local_dev",
#   hostname: "localhost",
#   pool_size: 10
# # ## SSL Support
#
# To get SSL working, you will need to add the `https` key
# to the previous section and set your `:url` port to 443:
#
#     config :hypert, HypertWeb.Endpoint,
#       ...
#       url: [host: "example.com", port: 443],
#       https: [:inet6,
#               port: 443,
#               keyfile: System.get_env("SOME_APP_SSL_KEY_PATH"),
#               certfile: System.get_env("SOME_APP_SSL_CERT_PATH")]
#
# Where those two env variables return an absolute path to
# the key and cert in disk or a relative path inside priv,
# for example "priv/ssl/server.key".
#
# We also recommend setting `force_ssl`, ensuring no data is
# ever sent via http, always redirecting to https:
#
#     config :hypert, HypertWeb.Endpoint,
#       force_ssl: [hsts: true]
#
# Check `Plug.SSL` for all available options in `force_ssl`.

# ## Using releases
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start the server for all endpoints:
#
#     config :phoenix, :serve_endpoints, true
#
# Alternatively, you can configure exactly which server to
# start per endpoint:
#
#     config :hypert, HypertWeb.Endpoint, server: true
#

# Finally import the config/prod.secret.exs
# which should be versioned separately.
import_config "prod.secret.exs"
