defmodule Hypert.Repo.Migrations.RemoveBreakDate do
  use Ecto.Migration

  def change do
      alter table(:links) do
          remove :break_date
      end
  end
end
