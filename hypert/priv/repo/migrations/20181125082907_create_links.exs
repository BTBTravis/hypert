defmodule Hypert.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :title, :string
      add :url, :string
      add :break, :string
      add :break_date, :naive_datetime
      add :last_viewed, :naive_datetime

      timestamps()
    end

  end
end
