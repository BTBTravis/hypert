defmodule Hypert.Repo do
  use Ecto.Repo, otp_app: :hypert

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    if opts[:load_from_system_env] do
      sys_env_config = [
        {:username, System.get_env("DB_USER") || raise "expected DB_USER"},
        {:password, System.get_env("DB_PASS") || raise "expected DB_PASS"},
        {:database, System.get_env("DB_DB") || raise "expected DB_DB"},
        {:hostname, System.get_env("DB_HOST") || raise "expected DB_HOST"},
        {:url, System.get_env("DATABASE_URL")},
        {:pool_size, System.get_env("DB_POOL") || 10}
      ]
      {:ok, sys_env_config ++ opts} # sys_env_config first so it overrides lookups in opts
    else
      {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
    end
  end
end
