defmodule Hypert.ReleaseTasks do
  def migrate do
    {:ok, _} = Application.ensure_all_started(:hypert)

    path = Application.app_dir(:hypert, "priv/repo/migrations")

    # TODO: add boot hook that calls this https://www.thegreatcodeadventure.com/run-ecto-migrations-in-production-distillery/
    Ecto.Migrator.run(Hypert.Repo, path, :up, all: true)
  end
end
