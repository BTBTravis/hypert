defmodule Hypert.Memory do
  @moduledoc """
  The Memory context.
  """

  import Ecto.Query, warn: false
  alias Hypert.Repo

  alias Hypert.Memory.Link

  @doc """
  Returns the list of links.

  ## Examples

      iex> list_links()
      [%Link{}, ...]

  """
  def list_links do
    Repo.all(Link)
  end


  @doc """
  Recursively find the possible dates if the future a
  link could be shown

  ## Examples

      iex> possible_dates(link, 10)
      [~D[2019-02-11], ~D[2019-02-09], ~D[2019-02-07], ...]

  """
  def possible_dates(link, count, dates \\ [])
  def possible_dates(_, count, dates) when count <= 1 do
    dates
  end

  def possible_dates(link, count, dates) do
     next = case List.first(dates) do
              nil -> next_view_date(link.break, NaiveDateTime.to_date(link.last_viewed))
              x -> next_view_date(link.break, x)
            end
    possible_dates(link, count - 1, [next | dates])
  end


  @doc """
  Increments a date by a break interval ex 1y, 2m, 3w, or 4d

  ## Examples

      iex> next_view_date(1d, ~D[2019-02-09])
      ~D[2019-02-10]

  """
  def next_view_date(break, start_date) do
    break_parts = Regex.named_captures(~r/^(?<num>\d+)(?<unit>[m|d|y|w]$)/, break)
    num = elem(Integer.parse(break_parts["num"]), 0)
    unit = break_parts["unit"]
    days  = case unit do
              "d" ->
                num
              "w" ->
                num * 7
              "m"->
                num * 30
              "y" ->
                num * 365
              _ ->
                0
            end
    Date.add(start_date, days)
  end

  @doc """
  Returns the list of links that are sloted for viewing today
  """
  def todays_links do
    today = NaiveDateTime.to_date(NaiveDateTime.utc_now())
    list_links()
    |> Enum.map(fn(link) -> {link, possible_dates(link, 500)} end)
    |> Enum.filter(fn(pair) -> today in elem(pair, 1) end)
    |> Enum.map(fn(pair) -> elem(pair, 0) end)
  end

  @doc """
  Gets a single link.

  Raises `Ecto.NoResultsError` if the Link does not exist.

  ## Examples

      iex> get_link!(123)
      %Link{}

      iex> get_link!(456)
      ** (Ecto.NoResultsError)

  """
  def get_link!(id), do: Repo.get!(Link, id)

  @doc """
  Creates a link with break_date set to current date

  ## Examples

      iex> create_link(%{field: value})
      {:ok, %Link{}}

      iex> create_link(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_link(attrs \\ %{}) do
    # yesterday = NaiveDateTime.add(NaiveDateTime.utc_now(), 86400 * -1, :second)
    now = NaiveDateTime.utc_now()
    final_attrs = attrs
    |> Map.put("last_viewed", now)

    %Link{}
    |> Link.changeset(final_attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a link.

  ## Examples

      iex> update_link(link, %{field: new_value})
      {:ok, %Link{}}

      iex> update_link(link, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_link(%Link{} = link, attrs) do
    link
    |> Link.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Link.

  ## Examples

      iex> delete_link(link)
      {:ok, %Link{}}

      iex> delete_link(link)
      {:error, %Ecto.Changeset{}}

  """
  def delete_link(%Link{} = link) do
    Repo.delete(link)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking link changes.

  ## Examples

      iex> change_link(link)
      %Ecto.Changeset{source: %Link{}}

  """
  def change_link(%Link{} = link) do
    Link.changeset(link, %{})
  end

  @doc """
  Updates a link's last_viewed time stamp to now
  """
  def view_link(%Link{} = link) do
    now = NaiveDateTime.utc_now()

    link
    |> Link.changeset(%{:last_viewed => now})
    |> Repo.update()
  end

  @doc """
  List the links that have not been viewed during their day
  """
  def backlog_links() do
    midnight = %{NaiveDateTime.utc_now() | hour: 0, minute: 0, second: 0}
    Repo.all(from l in Link,
      where: l.last_viewed < type(^midnight, :naive_datetime))
    |> Enum.map(fn(l) -> {l, next_view_date(l.break, NaiveDateTime.to_date(l.last_viewed))} end)
    |> Enum.filter(fn(pair) ->
      next_view_date = elem(pair, 1) # date
      case Date.compare(next_view_date, NaiveDateTime.to_date(midnight)) do
        :lt -> true
        _ -> false
      end
    end)
    |> Enum.map(fn(pair) -> elem(pair, 0) end)
  end

end
