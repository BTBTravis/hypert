defmodule Hypert.Memory.Link do
  use Ecto.Schema
  import Ecto.Changeset


  schema "links" do
    field :title, :string
    field :url, :string
    field :break, :string
    field :last_viewed, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:title, :url, :break, :last_viewed])
    |> validate_required([:title, :url, :break, :last_viewed])
    |> validate_format(:break, ~r/^\d+[m|d|w|y]$/)
  end
end
