defmodule Hypert.Memory.Suggest do
  def get_title_suggestion(url) do
    case validate_uri(url) do
      {:ok, valid_url} -> retrive_url_title(url)
      _ -> nil
    end
  end

  def validate_uri(str) do
    uri = URI.parse(str)
    case uri do
      %URI{scheme: nil} -> {:error, uri}
      %URI{host: nil} -> {:error, uri}
      %URI{path: nil} -> {:error, uri}
      uri -> {:ok, uri}
    end
  end

  def retrive_url_title(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        parsed_body = Regex.named_captures(~r/<title.*>(?<name>.*)<\/title>/, body)
        case parsed_body do
          %{"name" => title} -> title
        end
      _ -> nil
      # {:ok, %HTTPoison.Response{status_code: 404}} ->
      #   IO.puts "Not found :("
      # {:error, %HTTPoison.Error{reason: reason}} ->
      #   IO.inspect reason
    end
  end
end
