defmodule HypertWeb.Router do
  use HypertWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug HypertWeb.Plugs.BasicAuth, :apikey
  end

  scope "/api", HypertWeb do
    pipe_through :api
    post "/links", LinkController, :create
    post "/links/:id", LinkController, :update
    get "/links/view/:id", LinkController, :view
    get "/links/delete/:id", LinkController, :delete
    get "/links/todays", LinkController, :todays
    get "/links/backlog", LinkController, :backlog
    get "/links", LinkController, :index
    get "/links/:id", LinkController, :show
    #get "/search/links", LinkController, :search
    get "/suggest", SuggestController, :suggest_title
  end
end
