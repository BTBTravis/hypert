defmodule HypertWeb.Plugs.BasicAuth do
  import Plug.Conn

  @realm "Basic realm=\"API\""

  def init(default), do: default
  # def init(opts), do: opts

  def call(conn, config_val) do
    correct_key = HypertWeb.Endpoint.config(config_val, "xxxx")
    case get_req_header(conn, "authorization") do
      ["key " <> attempted_key] ->
        verify(conn, attempted_key, correct_key)
      _ ->
        unauthorized(conn)
    end
  end

  defp verify(conn, attempted_key, correct_key) do
    cond do
      attempted_key == correct_key -> conn
      true ->
        unauthorized(conn)
    end
  end

  defp unauthorized(conn) do
    conn
    |> put_resp_header("www-authenticate", @realm)
    |> send_resp(401, "unauthorized")
    |> halt()
  end
end
