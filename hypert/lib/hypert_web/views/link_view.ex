defmodule HypertWeb.LinkView do
  use HypertWeb, :view
  alias HypertWeb.LinkView

  def render("index.json", %{links: links}) do
    %{data: render_many(links, LinkView, "link.json")}
  end

  def render("show.json", %{link: link}) do
    %{data: render_one(link, LinkView, "link.json")}
  end

  def render("link.json", %{link: link}) do
    %{id: link.id,
      title: link.title,
      url: link.url,
      break: link.break
    }
  end
end
