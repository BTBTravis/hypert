defmodule HypertWeb.SuggestView do
  use HypertWeb, :view
  alias HypertWeb.SuggestView

  def render("suggestion.json", %{suggestion: sg}) do
    %{suggestion: sg}
  end
end
