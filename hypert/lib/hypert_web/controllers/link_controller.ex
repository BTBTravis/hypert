defmodule HypertWeb.LinkController do
  use HypertWeb, :controller

  alias Hypert.Memory
  alias Hypert.Memory.Link

  action_fallback HypertWeb.FallbackController

  def index(conn, _params) do
    links = Memory.list_links()
    render(conn, "index.json", links: links)
  end

  def create(conn, %{"link" => link_params}) do
    with {:ok, %Link{} = link} <- Memory.create_link(link_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", link_path(conn, :show, link))
      |> render("show.json", link: link)
    end
  end

  def show(conn, %{"id" => id}) do
    link = Memory.get_link!(id)
    render(conn, "show.json", link: link)
  end

  def update(conn, %{"id" => id, "link" => link_params}) do
    link = Memory.get_link!(id)

    with {:ok, %Link{} = link} <- Memory.update_link(link, link_params) do
      render(conn, "show.json", link: link)
    end
  end

  def view(conn, %{"id" => id}) do
    link = Memory.get_link!(id)

    with {:ok, %Link{} = link} <- Memory.view_link(link) do
      render(conn, "show.json", link: link)
    end
  end

  def todays(conn, _params) do
    links = Memory.todays_links()
    render(conn, "index.json", links: links)
  end

  def backlog(conn, _params) do
    links = Memory.backlog_links()
    render(conn, "index.json", links: links)
  end

  def delete(conn, %{"id" => id}) do
    link = Memory.get_link!(id)
    with {:ok, %Link{}} <- Memory.delete_link(link) do
      send_resp(conn, :no_content, "")
    end
  end
end
