defmodule HypertWeb.SuggestController do
  use HypertWeb, :controller

  alias Hypert.Memory
  alias Hypert.Memory.Suggest, as: Suggest

  action_fallback HypertWeb.FallbackController

  def suggest_title(conn, %{"url" => url}) do
    possible_title = Suggest.get_title_suggestion(url)
    case possible_title do
      nil -> render(conn, "suggestion.json", suggestion: false)
      _ -> render(conn, "suggestion.json", suggestion: possible_title)
    end
  end
end
